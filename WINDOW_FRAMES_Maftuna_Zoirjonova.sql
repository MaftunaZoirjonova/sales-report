WITH WeeklySales AS (
    SELECT
        time_id,
        SUM(amount_sold) AS weekly_amount
    FROM
        sh.sales
    WHERE
        EXTRACT(WEEK FROM time_id) IN (49, 50, 51) AND EXTRACT(YEAR FROM time_id) = 1999
    GROUP BY
        time_id
),
CenteredAvg AS (
    SELECT
        time_id,
        LAG(weekly_amount) OVER (ORDER BY time_id) AS prev_weekly_amount,
        weekly_amount AS current_weekly_amount,
        LEAD(weekly_amount) OVER (ORDER BY time_id) AS next_weekly_amount
    FROM
        WeeklySales
)
SELECT
    t.time_id,
    ws.weekly_amount,
    SUM(ws.weekly_amount) OVER (ORDER BY t.time_id) AS CUM_SUM,
    (prev_weekly_amount + current_weekly_amount + next_weekly_amount) / 3 AS CENTERED_3_DAY_AVG
FROM
    CenteredAvg t
JOIN
    WeeklySales ws ON t.time_id = ws.time_id
ORDER BY
    t.time_id;